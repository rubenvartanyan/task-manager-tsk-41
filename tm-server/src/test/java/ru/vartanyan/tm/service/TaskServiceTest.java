package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.dto.User;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Test
    @Category(DBCategory.class)
    public void addAllTasksTest() throws Exception {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertNotNull(taskService.findById(task1.getId()));
        Assert.assertNotNull(taskService.findById(task2.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void addTaskTest() throws Exception {
        final Task task = new Task();
        taskService.add(task);
        Assert.assertNotNull(taskService.findById(task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void clearTasksTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAllTasks() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        try {
            taskService.addAll(tasks);
        } catch (ru.vartanyan.tm.exception.system.NullObjectException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findTaskByIdTest() throws Exception {
        final Task task = new Task();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertNotNull(taskService.findById(taskId));
    }

    @Test
    @Category(DBCategory.class)
    public void findTaskByIndexTest() throws Exception {
        taskService.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertEquals(task, taskService.findByIndex(0, userId));
    }

    @Test
    @Category(DBCategory.class)
    public void findTaskByNameTest() throws Exception {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertEquals(task, taskService.findByName(name, userId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTaskByIdTest() throws Exception {
        final Task task1 = new Task();
        taskService.add(task1);
        final String taskId = task1.getId();
        taskService.removeById(taskId);
        Assert.assertNull(taskService.findById(taskId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTaskByNameTest() throws Exception {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskService.add(task);
        taskService.removeByName(name, userId);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTaskByIndexTest() throws Exception {
        taskService.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskService.add(task);
        taskService.removeByIndex(0, userId);
        Assert.assertNull(taskService.findById(name, taskId));
    }

}
