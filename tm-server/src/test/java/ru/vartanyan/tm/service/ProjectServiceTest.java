package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.User;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @SneakyThrows
    @Test
    @Category(DBCategory.class)
    public void addAllProjectsTest(){
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertNotNull(projectService.findById(project1.getId()));
        Assert.assertNotNull(projectService.findById(project2.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void addProjectTest() throws Exception {
        final Project project = new Project();
        projectService.add(project);
        Assert.assertNotNull(projectService.findById(project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void clearProjectsTest() {
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAllProjects() {
        projectService.clear();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        try {
            projectService.addAll(projects);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findProjectByIdTest() throws Exception {
        final Project project = new Project();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findById(projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void findProjectByIndexTest() throws Exception {
        projectService.clear();
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        Project project1 = projectService.findByIndex(0, userId);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findProjectByNameTest() throws Exception {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Project project1 = projectService.findByName(name, userId);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void removeProjectByIdTest() throws Exception {
        final Project project1 = new Project();
        projectService.add(project1);
        final String projectId = project1.getId();
        projectService.removeById(projectId);
        Assert.assertNull(projectService.findById(projectId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeProjectByNameTest() throws Exception {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectService.add(project);
        projectService.removeByName(name, userId);
    }

    @Test
    @Category(DBCategory.class)
    public void removeProjectByIndexTest() throws Exception {
        projectService.clear();
        final Project project = new Project();
        final String projectId = project.getId();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("project1");
        final String name = project.getName();
        projectService.add(project);
        projectService.removeByIndex(0, userId);
        Assert.assertNull(projectService.findById(name, projectId));
    }

}
