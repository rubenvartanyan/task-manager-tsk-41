package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.User;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Test
    @Category(DBCategory.class)
    public void addUserTest() throws Exception {
        final User user = new User();
        userService.add(user);
        Assert.assertNotNull(userService.findById(user.getId()));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void clearUsersTest() {
        userService.clear();
        try {
            Assert.assertTrue(userService.findAll().isEmpty());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Category(DBCategory.class)
    public void findAllUsers() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        try {
            userService.add(user1);
            userService.add(user2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Assert.assertEquals(2, userService.findAll().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Category(DBCategory.class)
    public void findUserByIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findById(userId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeUserByIdTest() throws Exception {
        final User user1 = new User();
        userService.add(user1);
        final String userId = user1.getId();
        userService.removeById(userId);
        Assert.assertNull(userService.findById(userId));
    }

}
