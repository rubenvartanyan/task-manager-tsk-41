package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    User findUserById(@WebParam(name = "id", partName = "id") @NotNull String id,
                  @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception, WrongRoleException;

    @WebMethod
    void addUser(@WebParam (name = "user", partName = "user") @NotNull final User user,
             @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    User findUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                            @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeUser(@WebParam (name = "user", partName = "user") @NotNull final User user,
                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeUserById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeUserByLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                       @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void createUser(@WebParam (name = "login", partName = "login") @NotNull final String login,
                         @WebParam (name = "password", partName = "password") @NotNull final String password,
                         @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void createUserWithEmail(@WebParam (name = "login", partName = "login") @NotNull final String login,
                @WebParam (name = "password", partName = "password") @NotNull final String password,
                @WebParam (name = "email", partName = "email") @NotNull final String email,
                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void createUserWithRole(@WebParam (name = "login", partName = "login") @NotNull final String login,
                @WebParam (name = "password", partName = "password") @NotNull final String password,
                @WebParam (name = "role", partName = "role") @NotNull final Role role,
                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void setUserPassword(@WebParam (name = "password", partName = "password") @NotNull final String password,
                     @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void updateUser(@WebParam (name = "userId", partName = "userId") @NotNull final String userId,
                    @WebParam (name = "firstName", partName = "firstName")@Nullable final String firstName,
                    @WebParam (name = "lastName", partName = "lastName") @Nullable final String lastName,
                    @WebParam (name = "middleName", partName = "middleName")@Nullable final String middleName,
                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void unlockUserByLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void unlockUserById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                        @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void lockUserByLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                         @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void lockUserById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                      @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @Nullable
    List<User> findAllUsers(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void clearUsers(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void addAllUsers(@WebParam (name = "userList", partName = "userList") final List<User> users,
                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

}
