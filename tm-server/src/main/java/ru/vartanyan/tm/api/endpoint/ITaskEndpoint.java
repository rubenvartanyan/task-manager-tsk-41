package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task findTaskById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                      @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeTaskById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                        @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

   @WebMethod
    @NotNull
    List<Task> findAllTasks(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

   @WebMethod
    void clearTasks(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @Nullable
    Task findTaskByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                         @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @Nullable
    Task findTaskByName(@WebParam (name = "name", partName = "name") @NotNull final String name,
                        @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeTaskByIndex(@WebParam (name = "index", partName = "index") @NotNull final Integer index,
                              @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeTaskByName(@WebParam (name = "name", partName = "name") @NotNull final String name,
                             @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void addTask(@WebParam (name = "name", partName = "name") @NotNull String name,
                    @WebParam (name = "description", partName = "description") @NotNull String description,
                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;


}
