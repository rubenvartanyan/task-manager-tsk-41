package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Task;

import java.util.List;

public interface IProjectTaskService {

    // SHOW ALL TASKS FROM PROJECT
    @Nullable
    List<Task> findAllTaskByProjectId(@NotNull final String projectId,
                                                @NotNull final String userId) throws Exception;

    // ADD TASK TO PROJECT
    void bindTaskByProjectId(@NotNull final String projectId,
                             @NotNull final String taskId,
                             @NotNull final String userId) throws Exception;

    // REMOVE TASK FROM PROJECT

    void unbindTaskFromProject(@NotNull final String projectId,
                               @NotNull final String taskId,
                               @NotNull final String userId) throws Exception;

    // REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT
    void removeProjectById(@NotNull final String projectId,
                           @NotNull final String userId) throws Exception;

}
