package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.dto.AbstractEntity;

import java.sql.SQLException;
import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    @Nullable List<E> findAll() throws Exception;

    void add(@NotNull E entity) throws Exception;

    void addAll(@Nullable List<E> entities) throws Exception;

    void clear() throws SQLException, Exception;

    void remove(@Nullable E entity) throws Exception;

    boolean contains(@NotNull String id) throws EmptyIdException, SQLException;

    @Nullable
    E findById(@Nullable String id) throws EmptyIdException, SQLException;

    void removeById(@Nullable String id) throws Exception;

}
