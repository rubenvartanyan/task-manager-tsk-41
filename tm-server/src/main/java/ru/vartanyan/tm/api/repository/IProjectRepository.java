package ru.vartanyan.tm.api.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.dto.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Insert("INSERT INTO `app_project` " +
            "(`id`, `name`, `description`, `user_id`, `created`, `date_start`, `date_finish`, `status`) " +
            "VALUES(" +
            "#{id}, #{name}, #{description}, #{userId}, #{created}, #{dateStart}, #{dateFinish}, #{status})")
    void add(@NotNull Project project);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Project findById(@Param("id") @Nullable String id);

    @SneakyThrows
    @Update("UPDATE `app_project` SET `status` = #{status} " +
            "WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void changeStatusById(
            @Param("id") @Nullable String id,
            @Param("userId") @Nullable String userId,
            @Param("status") @Nullable Status status);

    @Delete("DELETE FROM `app_project`")
    void clear();

    @Delete("DELETE FROM `app_project` WHERE `user_id` = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @Select("SELECT * FROM `app_project`")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    List<Project> findAll();

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `user_id` = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    List<Project> findAllByUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Project findByIdAndUserId(
            @Param("id") @NotNull String id,
            @Param("userId") @Nullable String userId
    );

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `user_id` = #{userId} LIMIT #{index}, 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Project findByIndex(
            @Param("index") @NotNull Integer index,
            @Param("userId") @Nullable String userId
    );

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_finish", property = "dateFinish")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    Project findByName(
            @Param("name") @NotNull String name,
            @Param("userId") @Nullable String userId
    );


    @Delete("DELETE FROM `app_project` WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void removeByIdAndUserId(@Param("id") @Nullable String id,
                             @Param("userId") @Nullable String userId);

    @Delete("DELETE FROM `app_project` WHERE `name` = #{name} AND `user_id` = #{userId} LIMIT 1")
    void removeByName(
            @Param("name") @NotNull String name,
            @Param("userId") @Nullable String userId
    );

    @SneakyThrows
    @Update("UPDATE `app_project` SET `name` = #{name}, `description` = #{description}" +
            " WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    void updateById(
            @Param("id") @Nullable String id,
            @Param("userId") @Nullable String userId,
            @Param("name") @Nullable String name,
            @Param("description") @Nullable String description
    );

    @Delete("DELETE FROM `app_project` WHERE `id` = #{id}")
    void removeById(@Param("id") @Nullable String id);

}
