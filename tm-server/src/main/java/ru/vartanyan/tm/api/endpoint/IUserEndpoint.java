package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    @Nullable
    User findUserByLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                         @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                              @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

}
