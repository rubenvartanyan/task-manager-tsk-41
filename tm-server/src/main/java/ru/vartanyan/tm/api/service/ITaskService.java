package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.dto.Task;

public interface ITaskService extends IBusinessService<Task> {

    void add(@NotNull final String name,
             @NotNull final String description,
             @NotNull final String userId) throws Exception;

    Task findByName(@Nullable final String name,
                    @Nullable final String userId) throws EmptyNameException, EmptyIdException;

    Task findByIndex(final int index,
                     @Nullable final String userId) throws IncorrectIndexException, EmptyIdException;

    void removeByName(@Nullable final String name,
                      @Nullable final String userId) throws EmptyNameException, EmptyIdException;

    void removeByIndex(final int index,
                       @Nullable final String userId) throws IncorrectIndexException, EmptyIdException, NullObjectException;

}
