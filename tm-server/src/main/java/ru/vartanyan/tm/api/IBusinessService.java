package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.dto.AbstractBusinessEntity;

import java.sql.SQLException;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    void updateEntityById(String id,
                          String name,
                          String description,
                          String userId) throws Exception;


    void updateEntityByIndex(@NotNull final Integer index,
                             @NotNull final String name,
                             @NotNull final String description,
                             @NotNull final String userId) throws Exception;

    void startEntityById(@NotNull final String id,
                         @NotNull final String userId) throws Exception;

    void startEntityByName(@NotNull final String name,
                           @NotNull final String userId) throws Exception;

    void startEntityByIndex(@NotNull final Integer index,
                            @NotNull final String userId) throws Exception;

    void finishEntityById(@NotNull final String id,
                          @NotNull final String userId) throws Exception;

    void finishEntityByName(@NotNull final String name,
                            @NotNull final String userId) throws Exception;

    void finishEntityByIndex(@NotNull final Integer index,
                             @NotNull final String userId) throws Exception;


    void updateEntityStatusById(@NotNull final String id,
                                @NotNull final Status status,
                                @NotNull final String userId) throws Exception;



    void updateEntityStatusByName(@NotNull final String name,
                                  @NotNull final Status status,
                                  @NotNull final String userId) throws Exception;

    void updateEntityStatusByIndex(@NotNull final Integer index,
                                   @NotNull final Status status,
                                   @NotNull final String userId) throws Exception;

    public void removeById(@Nullable final String userId,
                           @Nullable final String id) throws Exception;

    E findById(@Nullable final String id,
                            @Nullable final String userId) throws EmptyIdException, SQLException;

}
