package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.dto.Project;

public interface IProjectService extends IBusinessService<Project> {

    void add(@Nullable final String name,
             @Nullable final String description,
             @Nullable final String userId) throws Exception;

    Project findByName(@Nullable final String name,
                    @Nullable final String userId) throws EmptyNameException, EmptyIdException;

    Project findByIndex(final int index,
                     @Nullable final String userId) throws IncorrectIndexException, EmptyIdException;

    void removeByName(@Nullable final String name,
                      @Nullable final String userId) throws EmptyNameException, EmptyIdException;

    void removeByIndex(final int index,
                       @Nullable final String userId) throws IncorrectIndexException, EmptyIdException, NullObjectException;

}
