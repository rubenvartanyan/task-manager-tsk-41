package ru.vartanyan.tm.exception.system;

public class NullObjectException extends Exception {

    public NullObjectException() {
        super("Error! Project is not found...");
    }

}
