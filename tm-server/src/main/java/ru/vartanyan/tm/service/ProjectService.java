package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.dto.Project;


import java.sql.SQLException;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void add(final @Nullable String name,
                    final @Nullable String description,
                    final @Nullable String userId) throws Exception {
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Project findByName(@Nullable String name,
                              @Nullable String userId) throws EmptyNameException, EmptyIdException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project findByIndex(int index,
                               @Nullable String userId) throws IncorrectIndexException, EmptyIdException {
        if (index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findByIndex(index, userId);
    }

    @Override
    public void removeByName(@Nullable String name,
                             @Nullable String userId) throws EmptyNameException, EmptyIdException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeByName(name, userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public void removeByIndex(int index, @Nullable String userId)
            throws IncorrectIndexException, EmptyIdException, NullObjectException {
        if (index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable Project project = projectRepository.findByIndex(index, userId);
            if (project == null) throw new NullObjectException();
            projectRepository.removeByIdAndUserId(userId, project.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@Nullable final Project project) throws Exception {
        if (project == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final Project project) throws Exception {
        if (project == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeById(project.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id,
                            @Nullable final String userId) throws EmptyIdException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findByIdAndUserId(id, userId);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findById(id);
    }

    @Override
    public void removeById(@Nullable final String userId,
                           @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeByIdAndUserId(id, userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findAll();
    }

    public void clear() throws SQLException {
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void addAll(@Nullable final List<Project> list) throws Exception {
        if (list == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            list.forEach(projectRepository::add);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean contains(@Nullable String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return !(projectRepository.findById(id) == null);
    }

    @Override
    public void updateEntityById(String id,
                                 String name,
                                 String description,
                                 String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByIdAndUserId(id, userId);
        if (project == null) throw new NullObjectException();
        project.setName(name);
        project.setDescription(description);
        try {
            projectRepository.updateById(userId, project.getId(), name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityByIndex(@Nullable Integer index,
                                    @Nullable String name,
                                    @Nullable String description,
                                    @Nullable String userId) throws Exception {
        if (index == null) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByIndex(index, userId);
        if (project == null) throw new NullObjectException();
        project.setName(name);
        project.setDescription(description);
        try {
            projectRepository.updateById(userId, project.getId(), name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public void startEntityById(@Nullable String id,
                                @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = findById(id, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.IN_PROGRESS);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.changeStatusById(project.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void startEntityByName(@Nullable String name,
                                  @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByName(name, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.IN_PROGRESS);
        try {
            projectRepository.changeStatusById(project.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void startEntityByIndex(@Nullable Integer index,
                                   @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByIndex(index, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.IN_PROGRESS);
        try {
            projectRepository.changeStatusById(project.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityById(@Nullable String id,
                                 @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = findById(id, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.COMPLETE);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.changeStatusById(project.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityByName(@Nullable String name,
                                   @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByName(name, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.COMPLETE);
        try {
            projectRepository.changeStatusById(project.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityByIndex(@Nullable Integer index,
                                    @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByIndex(index, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(Status.COMPLETE);
        try {
            projectRepository.changeStatusById(project.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusById(@Nullable String id,
                                       @Nullable Status status,
                                       @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @Nullable final Project project = findById(id, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.changeStatusById(project.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusByName(@Nullable String name,
                                         @Nullable Status status,
                                         @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByName(name, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(status);
        try {
            projectRepository.changeStatusById(project.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusByIndex(@Nullable Integer index,
                                          @Nullable Status status,
                                          @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findByIndex(index, userId);
        if (project == null) throw new NullObjectException();
        project.setStatus(status);
        try {
            projectRepository.changeStatusById(project.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
