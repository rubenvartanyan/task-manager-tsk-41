package ru.vartanyan.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.dto.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService{

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }
    @NotNull
    @Override
    public final List<Task> findAllTaskByProjectId(@Nullable final String projectId,
                                                    @Nullable final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void bindTaskByProjectId(@Nullable final String projectId,
                                    @Nullable final String taskId,
                                    @NotNull final String userId) throws Exception{
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String projectId,
                                      @Nullable final String taskId,
                                      @NotNull final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.unbindTaskFromProject(taskId, userId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String projectId,
                                  @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeByIdAndUserId(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
