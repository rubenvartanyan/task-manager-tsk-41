package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
