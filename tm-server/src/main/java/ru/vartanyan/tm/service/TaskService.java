package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.dto.Task;

import java.sql.SQLException;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public Task findByName(@Nullable String name,
                              @Nullable String userId) throws EmptyNameException, EmptyIdException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task findByIndex(int index,
                               @Nullable String userId) throws IncorrectIndexException, EmptyIdException {
        if (index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByIndex(index, userId);
    }

    @Override
    public void removeByName(@Nullable String name,
                             @Nullable String userId) throws EmptyNameException, EmptyIdException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeByName(name, userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public void removeByIndex(int index, @Nullable String userId)
            throws IncorrectIndexException, EmptyIdException, NullObjectException {
        if (index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable Task task = taskRepository.findByIndex(index, userId);
            if (task == null) throw new NullObjectException();
            taskRepository.removeByIdAndUserId(userId, task.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@NotNull final String name,
                    @NotNull final String description,
                    @NotNull final String userId) throws Exception {
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@Nullable final Task task) throws Exception {
        if (task == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final Task task) throws Exception {
        if (task == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(task.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id,
                            @Nullable final String userId) throws EmptyIdException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findById(id);
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findById(id);
    }

    @Override
    public void removeById(@Nullable final String userId,
                           @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll() throws Exception {
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAll();
    }

    public void clear() throws SQLException {
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void addAll(@Nullable final List<Task> list) throws Exception {
        if (list == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            list.forEach(taskRepository::add);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean contains(@Nullable String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return !(taskRepository.findById(id) == null);
    }

    @Override
    public void updateEntityById(String id,
                                 String name,
                                 String description,
                                 String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findById(id);
        if (task == null) throw new NullObjectException();
        task.setName(name);
        task.setDescription(description);
        try {
            taskRepository.updateById(userId, task.getId(), name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityByIndex(@Nullable Integer index,
                                    @Nullable String name,
                                    @Nullable String description,
                                    @Nullable String userId) throws Exception {
        if (index == null) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByIndex(index, userId);
        if (task == null) throw new NullObjectException();
        task.setName(name);
        task.setDescription(description);
        try {
            taskRepository.updateById(userId, task.getId(), name, description);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public void startEntityById(@Nullable String id,
                                @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findById(id, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.IN_PROGRESS);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.changeStatusById(task.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void startEntityByName(@Nullable String name,
                                  @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByName(name, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.IN_PROGRESS);
        try {
            taskRepository.changeStatusById(task.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void startEntityByIndex(@Nullable Integer index,
                                   @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByIndex(index, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.IN_PROGRESS);
        try {
            taskRepository.changeStatusById(task.getId(), userId, Status.IN_PROGRESS);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityById(@Nullable String id,
                                 @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = findById(id, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.COMPLETE);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.changeStatusById(task.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityByName(@Nullable String name,
                                   @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByName(name, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.COMPLETE);
        try {
            taskRepository.changeStatusById(task.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void finishEntityByIndex(@Nullable Integer index,
                                    @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByIndex(index, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(Status.COMPLETE);
        try {
            taskRepository.changeStatusById(task.getId(), userId, Status.COMPLETE);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusById(@Nullable String id,
                                       @Nullable Status status,
                                       @Nullable String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @Nullable final Task task = findById(id, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.changeStatusById(task.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusByName(@Nullable String name,
                                         @Nullable Status status,
                                         @Nullable String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByName(name, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(status);
        try {
            taskRepository.changeStatusById(task.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateEntityStatusByIndex(@Nullable Integer index,
                                          @Nullable Status status,
                                          @Nullable String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findByIndex(index, userId);
        if (task == null) throw new NullObjectException();
        task.setStatus(status);
        try {
            taskRepository.changeStatusById(task.getId(), userId, status);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
