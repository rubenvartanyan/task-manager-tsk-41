package ru.vartanyan.tm.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="yourRootElementName")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Entity(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Column
    @Nullable Long timestamp = System.currentTimeMillis();

    @Column(name = "user_id")
    @Nullable String userId;

    @Column
    @Nullable String signature;

    public Session() {
    }

}