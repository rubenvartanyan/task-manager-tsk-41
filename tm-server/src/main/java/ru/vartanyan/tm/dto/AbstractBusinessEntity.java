package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")
@XmlAccessorType(XmlAccessType.FIELD)

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity{

    @Column
    @NotNull public String name = "";

    @Column
    @NotNull public String description = "";

    @Column(name = "user_id")
    @NotNull public String userId;

    @Enumerated(EnumType.STRING)
    @NotNull public Status status = Status.NOT_STARTED;

    @Column(name = "date_started")
    @Nullable public Date dateStarted;

    @Column(name = "date_finish")
    @Nullable public Date dateFinish;

    @Column
    @NotNull public Date created = new Date();

    @Override
    public String toString() {
        return id + ": " + name + "; " + description + "; " + "User Id: " + getUserId();
    }

}
