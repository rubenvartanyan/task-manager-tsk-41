package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_task")
public class Task extends AbstractBusinessEntity implements IWBS {

    @Column(name = "project_id")
    @Nullable private String projectId;

    public @Nullable String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateStarted=" + dateStarted +
                ", dateFinish=" + dateFinish +
                ", created=" + created +
                ", projectId='" + projectId + '\'' +
                '}';
    }

}
