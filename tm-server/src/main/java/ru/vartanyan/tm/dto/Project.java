package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;

import javax.persistence.Entity;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "app_project")
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull final String name,
                   @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateStarted=" + dateStarted +
                ", dateFinish=" + dateFinish +
                ", created=" + created +
                '}';
    }

}
