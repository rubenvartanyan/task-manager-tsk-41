package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IUserEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public User findUserByLogin(@WebParam(name = "login", partName = "login") @NotNull final String login,
                                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        try {
            serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        } catch (ru.vartanyan.tm.exception.system.WrongRoleException e) {
            e.printStackTrace();
        }
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    @Override
    public void removeUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                                     @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        try {
            serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        } catch (ru.vartanyan.tm.exception.system.WrongRoleException e) {
            e.printStackTrace();
        }
        serviceLocator.getUserService().removeByLogin(login);
    }

}
