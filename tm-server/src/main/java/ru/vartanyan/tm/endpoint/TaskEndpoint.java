package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.Task;
import ru.vartanyan.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="yourRootElementName")


@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    
    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    @Override
    public Task findTaskById(@WebParam(name = "id", partName = "id") @NotNull String id,
                             @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(id, session.getUserId());
    }

    @WebMethod
    @Override
    public void removeTaskById(@WebParam (name = "id", partName = "id") @NotNull String id,
                               @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeById(id, session.getUserId());
    }

    @WebMethod
    @NotNull
    @Override
    public List<Task> findAllTasks(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    @Override
    public void clearTasks(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear();
    }

    @Nullable
    @WebMethod
    @Override
    public Task findTaskByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(index, session.getUserId());
    }

    @WebMethod
    @Nullable
    @Override
    public Task findTaskByName(@WebParam (name = "name", partName = "name") @NotNull String name,
                               @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(name, session.getUserId());
    }

    @WebMethod
    @Override
    public void removeTaskByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                                  @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByIndex(index, session.getUserId());
    }

    @WebMethod
    @Override
    public void removeTaskByName(@WebParam (name = "name", partName = "name") @NotNull String name,
                                 @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByName(name, session.getUserId());
    }

    @WebMethod
    @Override
    public void addTask(@WebParam (name = "name", partName = "name") @NotNull String name,
                           @WebParam (name = "description", partName = "description") @NotNull String description,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(name, description, session.getUserId());

    }

}
