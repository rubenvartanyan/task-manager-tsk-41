package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-remove-by-login";
    }

    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        endpointLocator.getAdminEndpoint().removeUserByLogin(login, session);
        System.out.println("[USER REMOVED]");
    }

}
