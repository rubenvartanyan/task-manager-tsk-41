package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[REMOVE PROJECT BY NAME");
        System.out.println("[ENTER NAME");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().removeProjectByName(name, session);
        System.out.println("[PROJECT REMOVED]");
    }

}
