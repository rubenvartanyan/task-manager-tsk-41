package ru.vartanyan.tm.exception.system;

public class AccessDeniedException extends Exception{

    public AccessDeniedException()  {
        super("Error! Access denied...");
    }

}
