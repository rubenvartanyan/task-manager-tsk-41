package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class AdminEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        User user = new User();
        user.setLogin("AAA");
        endpointLocator.getAdminEndpoint().addUser(user, sessionAdmin);
        endpointLocator.getAdminEndpoint().removeUserByLogin("AAA", sessionAdmin);
        Assert.assertNull(endpointLocator.getAdminEndpoint().findUserByItsLogin("AAA", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByIdTest() {
        endpointLocator.getAdminEndpoint().createUser("XXX", "XXX", sessionAdmin);
        final User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("XXX", sessionAdmin);
        final String userId = user.getId();
        final User userFound = endpointLocator.getAdminEndpoint().findUserById(userId, sessionAdmin);
        Assert.assertEquals("XXX", userFound.getLogin());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        endpointLocator.getAdminEndpoint().createUser("MMM", "MMM", sessionAdmin);
        final User userFound = endpointLocator.getAdminEndpoint().findUserByItsLogin("MMM", sessionAdmin);
        Assert.assertNotNull(userFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithEmailTest() {
        endpointLocator.getAdminEndpoint().createUserWithEmail("test1", "test1", "test@tsk.ru", sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        endpointLocator.getAdminEndpoint().createUser("login1", "password1", sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRoleTest() {
        endpointLocator.getAdminEndpoint().createUserWithRole("QQQQ", "password1", Role.USER, sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("QQQQ", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        endpointLocator.getAdminEndpoint().createUser("EEE", "EEE", sessionAdmin);
        User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("EEE", sessionAdmin);
        endpointLocator.getAdminEndpoint().lockUserByLogin("EEE", sessionAdmin);
        Assert.assertTrue(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByIdTest() {
        endpointLocator.getAdminEndpoint().createUser("RRR", "RRR", sessionAdmin);
        User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("RRR", sessionAdmin);
        endpointLocator.getAdminEndpoint().lockUserById(user.id, sessionAdmin);
        Assert.assertTrue(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        endpointLocator.getAdminEndpoint().createUser("FFF", "FFF", sessionAdmin);
        User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("FFF", sessionAdmin);
        endpointLocator.getAdminEndpoint().lockUserByLogin("FFF", sessionAdmin);
        endpointLocator.getAdminEndpoint().unlockUserByLogin("FFF", sessionAdmin);
        Assert.assertFalse(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByIdTest() {
        endpointLocator.getAdminEndpoint().createUser("KKK", "KKK", sessionAdmin);
        final User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("KKK", sessionAdmin);
        final String userId = user.getId();
        endpointLocator.getAdminEndpoint().lockUserById(userId, sessionAdmin);
        endpointLocator.getAdminEndpoint().unlockUserById(userId, sessionAdmin);
        Assert.assertFalse(user.locked);
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        endpointLocator.getAdminEndpoint().createUser("ZZZ", "SSS", sessionAdmin);
        final User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("ZZZ", sessionAdmin);
        final String userId = user.getId();
        endpointLocator.getAdminEndpoint().removeUserById(userId, sessionAdmin);
        Assert.assertNull(endpointLocator.getAdminEndpoint().findUserById(userId, sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        endpointLocator.getAdminEndpoint().createUser("DDD", "DDD", sessionAdmin);
        final User user = endpointLocator.getAdminEndpoint().findUserByItsLogin("DDD", sessionAdmin);
        endpointLocator.getAdminEndpoint().removeUserByLogin("DDD", sessionAdmin);
        Assert.assertNull(endpointLocator.getAdminEndpoint().findUserByItsLogin("DDD", sessionAdmin));
    }

}
